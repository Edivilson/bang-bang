#include "GLWidget.h"
#include <QTimer>
#include <QDateTime>
#include <math.h>
#include <QDebug>

void GLWidget::_tick()
{
    /* Decide which canvas should be updated */

    /*qint64 now = QDateTime::currentMSecsSinceEpoch();

    // 60 FPS canvas must be draw every time, so we ignore _counter
    {
        // Compute how much time passed since the last update
        qint64 passed = now - _last_time_60fps;

        _accumulator60 += passed;
        while (_accumulator60 >= _constant_dt)
        {
            move(_constant_dt);
            _accumulator60 -= _constant_dt;
        }

        draw();
        _last_time_60fps = now;
    }

    _counter++;
    _counter = _counter % 60; // Ranges from 0 to 59*/

    updateModel();
    checkEdges();
    draw();


    // Reset the timer
    QTimer::singleShot(1000/_max_fps, this, SLOT(_tick()));
}

void GLWidget::move(float dt) {
/*
    if (_vel.x() > 0)
        _loc.setX(_loc.x() + (_vel.x() * dt * 60) / 1000);    // (2 * 16 * 60) / 1000 = 1.92 = 2
    else
        _loc.setX(_loc.x() + (_vel.x() * dt * 60) / 1000);   // (-2 * 16 * 60) / 1000 = -1.92 = -2

    if (_vel.y() > 0)
       _loc.setY(_loc.y() + (_vel.y() * dt * 60) / 1000);    // (1 * 16 * 60) / 1000 = 0.96 = 1
    else
       _loc.setY(_loc.y() + (_vel.y() * dt * 60) / 1000);   // (-1 * 16 * 60) / 1000 = -0.96 = -1



    if (_loc.x() <= -_w + _sq_sz || (_loc.x() >= _w - _sq_sz) ) {
        _vel.setX(_vel.x() * -1);
    }

    if (_loc.y() <= -_h + _sq_sz || (_loc.y() >= _h - _sq_sz) ) {
        _vel.setY(_vel.y() * -1);
    }*/
}

void GLWidget::draw() {
    update();
}

void GLWidget::applyForce(QVector2D force) {
      force.setX(force.x() / _mass);
      force.setY(force.y() / _mass);
      _acc += force;
}
void GLWidget::updateModel() {
    _vel += _acc;
    _loc += _vel;
    _acc *= QVector2D(0, 0);
}

void GLWidget::checkEdges() {
        if (_loc.x() > _w) {
            _loc.setX(_w);
            _vel.setX(_vel.x() * -1);
        } else if (_loc.x() < (_w*-1)) {
            _vel.setX(_vel.x() * -1);
            _loc.setX(_w*-1);
        }

        if (_loc.y() < (_h * -1)) {
            _vel.setY(_vel.y() * -1);
            _loc.setY(_h * -1);
        }

}
