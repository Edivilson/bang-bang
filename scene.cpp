#include "GLWidget.h"
#include <QDebug>

void GLWidget::_draw_scene()
{
  //  applyForce(_wind);
    applyForce(_gravity);

    drawBackground();
    _draw_cannon();
    _draw_ball();
}
