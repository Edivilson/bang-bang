#pragma once
#include <QGLWidget>
#include <iostream>
#include "glm.h"
#include <QVector2D>
#include <QImage>

class GLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit GLWidget(QWidget* parent = 0);
    virtual ~GLWidget();

    /* OpenGL initialization, viewport resizing, and painting */

    void initializeGL();
    void paintGL();
    void resizeGL( int width, int height);

    void _draw_scene();
    void _draw_ball();
    void _draw_cannon();

    void initializeBall();
    void initializeCannon();


    void move(float dt);
    void draw();

    void loadBackground();
    void drawBackground();

    void applyForce(QVector2D force);
    void updateModel();
    void checkEdges();


    /* enable the user to interact directly with the scene using the keyboard */

    void keyPressEvent(QKeyEvent *e);

private:
    int _width, _height;
    GLMmodel* _ball;
    GLMmodel* _cannon;

    int _max_fps;
    int _counter;
    float _constant_dt;
    qint64 _last_time_60fps, _last_time_30fps, _last_time_10fps;
    qint64 _accumulator60, _accumulator30, _accumulator10;
    int _sq_sz;
    float _w, _h;

    float _mass;
    QVector2D _loc;
    QVector2D _vel;
    QVector2D _acc;
    QVector2D _wind;
    QVector2D _gravity;

    QImage _background;

protected slots:
    void _tick();
};




