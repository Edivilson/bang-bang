#include <GL/glew.h>
#include <QDebug>
#include "GLWidget.h"


void GLWidget::initializeBall() {
    /* Load model with GLM */

    _ball = glmReadOBJ((char*)"data/bola.obj");

    if (!_ball)
    {
        std::cout << "GLWidget::initializeGL: !!! failed loading model from file" << std::endl;
        exit(0);
        return;
    }

    // This will rescale the object to fit into the unity matrix
    // Depending on your project you might want to keep the original size and positions you had in 3DS Max or GMAX so you may have to comment this.
    glmUnitize(_ball);

    // These 2 functions calculate triangle and vertex normals from the geometry data.
    // To be honest I had some problem with very complex models that didn't look to good because of how vertex normals were calculated
    // So if you can export these directly from you modeling tool do it and comment these line
    // 3DS Max can calculate these for you and GLM is perfectly capable of loading them
    glmFacetNormals(_ball);
    //glmVertexNormals(_ball, 90.0);
    glmVertexNormals(_ball, 180.0); //Optional-recalculates and smooths the normals
}

void GLWidget::_draw_ball()
{
    glPushMatrix();

    glTranslatef(_loc.x(), _loc.y(), 1.0f);

    //glColor3ub(255, 255, 0);
    glmDraw(_ball, GLM_SMOOTH | GLM_MATERIAL /*| GLM_TEXTURE */);

    glPopMatrix();
}
