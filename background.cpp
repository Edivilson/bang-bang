#include "glwidget.h"

static GLuint texName;

void GLWidget::loadBackground() {
    glDeleteTextures(1, &texName);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &texName);

    glBindTexture(GL_TEXTURE_2D, texName);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    _background.load("images/back1.png");
//      _background.load("images/back2.png");

    if(_background.isNull()) {
        std::cout << "Erro ao carregar imagem de background!";
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _background.width(),
                _background.height(), 1, GL_BGRA, GL_UNSIGNED_BYTE,
                _background.bits());
}

void GLWidget::drawBackground() {

    glPushMatrix();
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glBindTexture(GL_TEXTURE_2D, texName);

    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 1.0);
        glVertex3f(-_width/80, -_height/78.5, 0.0f);
        glTexCoord2f(1.0, 1.0);
        glVertex3f( _width/80, -_height/78.5, 0.0f);
        glTexCoord2f(1.0, 0.0);
        glVertex3f(_width/80, _height/78.5, 0.0f);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-_width/80, _height/78.5, 0.0f);
    glEnd();

    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}
