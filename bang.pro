QT += widgets opengl
LIBS += -lglew32

SOURCES += \
    main.cpp \
    glwidget.cpp \
    glm.cpp \
    events.cpp \
    scene.cpp \
    anim.cpp \
    background.cpp \
    ball.cpp \
    cannon.cpp

HEADERS += \
    glwidget.h \
    glm.h
