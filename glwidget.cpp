#include <QDateTime>
#include <GL/glew.h>
#include "GLWidget.h"

GLWidget::GLWidget(QWidget *parent)
: QGLWidget(parent)
{
    _width = 0;
    _height = 0;
    _ball = NULL;

    _counter = 0;
    _accumulator60 = _accumulator30 = _accumulator10 = 0;
    _max_fps = 60;
    _constant_dt = 1000 / _max_fps;
    _last_time_60fps = _last_time_30fps = _last_time_10fps = QDateTime::currentMSecsSinceEpoch();

    // Initialize class members
    _loc = QVector2D(-10, 10);
    _vel = QVector2D(0.0, 0.0);
    _acc = QVector2D(0, 0);
    _wind = QVector2D(0.01, 0);
    _gravity = QVector2D(0, -0.1);
    _mass = 1;

    _sq_sz = 0;
    _w = 10;
    _h = 8;
}

GLWidget::~GLWidget()
{
    if (_ball)
        glmDelete(_ball);
}

void GLWidget::initializeGL()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);				// Black Background
    glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
    glClearDepth(1.0f);									// Depth Buffer Setup
    glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
    glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do

    /* Anti-aliasing */

    /*
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
    */

    /* Enable lightning and light - GLM needs it */

    //glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    initializeBall();
    initializeCannon();
    loadBackground();

    /* Start the timer */

    _tick();
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    gluLookAt(0.0f,0.0f, 10.0f,
              0.0f, 0.0f, 0.0f,
              0.0f, 1.0f, 0.0f);

    _draw_scene();

    glPopMatrix();

    glFlush();
}

void GLWidget::resizeGL( int w, int h)
{
    _width = w;
    _height = h;

    glViewport     ( 0, 0, w, h );
    glMatrixMode   ( GL_PROJECTION );
    glLoadIdentity ( );

    if ( h==0 )  // Calcula Aspect Ratio da janela
       gluPerspective( 80, ( float ) w, 1.0, 100.0 );
    else
       gluPerspective ( 80, ( float ) w / ( float ) h, 1.0, 100.0 );

}


